// ==UserScript==
// @name        lorify
// @namespace   https://bitbucket.org/b0r3d0m/lorify-userscript
// @description lorify provides you an autorefresh for threads and an easy way to view referenced comments on linux.org.ru's forums
// @include     /^https?:\/\/www.linux.org.ru\/forum\/.*$/
// @include     /^https?:\/\/www.linux.org.ru\/gallery\/.*$/
// @include     /^https?:\/\/www.linux.org.ru\/news\/.*$/
// @include     /^https?:\/\/www.linux.org.ru\/polls\/.*$/
// @version     1.5.0
// @grant       none
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js
// @icon        http://icons.iconarchive.com/icons/icons8/christmas-flat-color/32/penguin-icon.png
// ==/UserScript==

var FAVICON_NOTIFY = "data:image/x-icon;base64,AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8BAAAAAAD//wEAAAAAAAAAAAAAAAAAAAAAQEAABBQAAA0AAAAVAAAAEQAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//AAEzmZkFAAAACgAAABcAAAAzAAAAUQAAAFkDAABdAAwXQgAAABcAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADDAwMFgAAAC0OJSo3HlZtRAtahl0AAC1EAAAALwAAADUXS2dmCUZwkhhhi60xjbbSHWSQ1g5TgbwQOFFxAAAAPgkiMx4AAAADAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAUPLj4hAAAAQx5df5kykLnvOZ3K/0KfwfdCn7q9I2iGkw80S3oITHmlLY258Ei95v8ogLHIAFqaWxtKXiYAAAAgDRsoEwAAAAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAATNmZgUAAAASGF2AYCyGsadIq8mmYdHnwF3a+/86p9XTAAAAKAAAABMml8mbKavj2hqAvxQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAADKKjiyiKa3asAAAAAAAAAAByZ34AOmd7bAIDVDAAAAAAAgNUGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAWYfQAEx42wAAABgAAAADAEpvtwxhif8WOldGAAAAAABVgAYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWZmZgUAAAAAAAAARSYiFfsRBgD/AAAA0Ts4O8Nwamf4Ukc9/wAAAKIAAAAAAAAAAAAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVVVUDAAAAAAAAAA+SkZHHysfH/5yVlf2EeXr/l4uM/7itrf+km53/Qj9A+gAAAEwAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP///wMAAAAAkZGRffLy8v/18vL/yL/A/LSmpvuhk4z6fXBr/paMivp8dXX/AAAAwRISEg4AAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAgIAEAAAAAAAAACfS0tPk//////Hv8Pvm4+L/19HQ/3lws/8AAIz/ODNS/GVeWf8vKyf/AAAAVAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAICAgAQAAAAAAAAAeKKin//4+Pj//P3+/P/////V1OX/JRjd/wAA9f8AALT/Jh5e+yklJf8AAACNAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAEAAAAADUwijAAAHbcMzJn/93d2/z//////////6inwf8AALP/HQPr/wAA4P8PAKX6KSZt/xEPOaYAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAgAAAAAAAAACAACmrxAA9/8AALr9uLjD/v//////////1tbd/0E/h/8AAJL/JyGs/5mYz/pubJ//AAAcr///AAIAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABVVVUGAAAAADk5L0w7OKH9Myf//wAA1/qSkbH//v77////////////ysbU/3Rpiv+DfZ3/razO+1VUdv8AAADlS0tGLAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGZmmQUAAAAAZWVfK3h3sONCPNz/QDy9+tDQ2f/y8vD////////+/v/n4uH/zcS//9LL0P+TkqL+Njc4/yIiGv8AAABhAAAAAAAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMAAAAANzGKYl9dkf/a2uv//////PHx8f/+/v////////Du7//t6uv/6efm/5CQjP1FRUT/KCgq/wAAAFAAAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAd3dikv//+////////v7+/f/////+/v7//Pz8///////T09T/SkpL+wAAAP8JCgnrKSkpLAAAAAAAAAAFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAbW1tBwAAAAAAAAA2vby98/v5+f/v7Oz76+jo//Hv7//08fH/6ufn/5yZmf8lJSX6DAsL/w8PD84hISEXAAAAAAAAAAUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAClRUFDl5OPj/+rn5/ve29v/9/b2//z7+//t6uv/i4mJ/wAAAPkRERH/HBwcmgAAAAAAAAAAAAAAAwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEBAQAQAAAAAAAAALUBBQfPu7u7/////+fDx8f/9/f7///////b39v91dXX8AAAA/x4eHv8YGBhqAAAAAAAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAACSkpIHAAAAsbi5uf///////v7+/fn4+P//////7+/v/WZmZv9RUVH/ZWVllwAAAA8AAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAAAAAAASKCsvlNbW1/3//////Pv5/v////78/P3+hYWG/xgYGJ+ZmZn/mZmZ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgICABAAAAAAAAAAAsaeitMDd9/+HtuD97vP4+f////+ZmZn/mZmZ/5mZmf8Y3An/GNwJ/5mZmf+ZmZn/mZmZ/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD//wEAAAAAJNv/B1LJ60tMgZ7PI4rD/wCNyP9ips35kai8/5mZmf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/xjcCf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//AQAAAAAAAAAANKbWiiKn3f8modT/KaHX/gCMz/yZmZn/mZmZ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQIC/BAAAAAByeI0xRo2+9B6a3v8xlNP9WJK//pmZmf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/5mZmf8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACqqqoDAAAAAKylmkexr678eJKp/2+Elf3DwLz/mZmZ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/mZmZ/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKqqqgMAAAAAZ2tvRYyLiv1oYVr/Qzsy/Hx8fPqZmZn/mZmZ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/GNwJ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwAAAAAAAAAZAAAA5C8yNP8wMjT8AAAA/QAAAP+ZmZn/GNwJ/xjcCf8Y3An/GNwJ/xjcCf8Y3An/mZmZ/wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAA1NTVqNTU1/A8PD/8XFxb/ISEh3ZmZmf+ZmZn/mZmZ/xjcCf8Y3An/mZmZ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAHBwdHHR0dhBgYGHYcHBwkAAAAAAAAAACZmZn/mZmZ/5mZmf+ZmZn/AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/////////////g///wIf//+DP///8z////M////wH///4B///+AP///AD///wAf//4AH//8AB///AAf//wAH//+AB///gAf//8AH///AB///wA///8AP///gAf//8AB///AAf//gAD//8AA///AAP//wAD//8AB///gAf//94f8=";

var FAVICON_ORIGINAL = "data:image/x-icon;base64,AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8B////AAD//wH///8A////AP///wD///8AQEAABBQAAA0AAAAVAAAAEQAAAAX///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP//AAEzmZkFAAAACgAAABcAAAAzAAAAUQAAAFkDAABdAAwXQgAAABcAAAAE////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAADDAwMFgAAAC0OJSo3HlZtRAtahl0AAC1EAAAALwAAADUXS2dmCUZwkhhhi60xjbbSHWSQ1g5TgbwQOFFxAAAAPgkiMx4AAAAD////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AAAAAAUPLj4hAAAAQx5df5kykLnvOZ3K/0KfwfdCn7q9I2iGkw80S3oITHmlLY258Ei95v8ogLHIAFqaWxtKXiYAAAAgDRsoEwAAAAL///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAATNmZgUAAAASGF2AYCyGsadIq8mmYdHnwF3a+/86p9XTAAAAKAAAABMml8mbKavj2hqAvxT///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wAAAAADKKjiyiKa3av///8A////AByZ34AOmd7bAIDVDP///wAAgNUG////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wD///8A////AP///wAAWYfQAEx42wAAABgAAAADAEpvtwxhif8WOldG////AABVgAb///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAWZmZgX///8AAAAARSYiFfsRBgD/AAAA0Ts4O8Nwamf4Ukc9/wAAAKL///8A////AAAAAAP///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wBVVVUD////AAAAAA+SkZHHysfH/5yVlf2EeXr/l4uM/7itrf+km53/Qj9A+gAAAEz///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wP///8AkZGRffLy8v/18vL/yL/A/LSmpvuhk4z6fXBr/paMivp8dXX/AAAAwRISEg7///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wCAgIAE////AAAAACfS0tPk//////Hv8Pvm4+L/19HQ/3lws/8AAIz/ODNS/GVeWf8vKyf/AAAAVP///wAAAAAE////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AICAgAT///8AAAAAeKKin//4+Pj//P3+/P/////V1OX/JRjd/wAA9f8AALT/Jh5e+yklJf8AAACN////AP///wAAAAAC////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAIAE////ADUwijAAAHbcMzJn/93d2/z//////////6inwf8AALP/HQPr/wAA4P8PAKX6KSZt/xEPOab///8A////AAAAAAT///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAACAAv///wAAAAACAACmrxAA9/8AALr9uLjD/v//////////1tbd/0E/h/8AAJL/JyGs/5mYz/pubJ//AAAcr///AAL///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wBVVVUG////ADk5L0w7OKH9Myf//wAA1/qSkbH//v77////////////ysbU/3Rpiv+DfZ3/razO+1VUdv8AAADlS0tGLP///wAAAAAE////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AGZmmQX///8AZWVfK3h3sONCPNz/QDy9+tDQ2f/y8vD////////+/v/n4uH/zcS//9LL0P+TkqL+Njc4/yIiGv8AAABh////AAAAAAIAAAAB////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AAAAAAP///8ANzGKYl9dkf/a2uv//////PHx8f/+/v////////Du7//t6uv/6efm/5CQjP1FRUT/KCgq/wAAAFD///8AAAAAA////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAv///wD///8Ad3dikv//+////////v7+/f/////+/v7//Pz8///////T09T/SkpL+wAAAP8JCgnrKSkpLP///wAAAAAF////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AbW1tB////wAAAAA2vby98/v5+f/v7Oz76+jo//Hv7//08fH/6ufn/5yZmf8lJSX6DAsL/w8PD84hISEX////AAAAAAX///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAE////AAAAAClRUFDl5OPj/+rn5/ve29v/9/b2//z7+//t6uv/i4mJ/wAAAPkRERH/HBwcmv///wD///8AAAAAA////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AEBAQAT///8AAAAALUBBQfPu7u7/////+fDx8f/9/f7///////b39v91dXX8AAAA/x4eHv8YGBhq////AAAAAAIAAAAB////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAv///wCSkpIHAAAAsbi5uf///////v7+/fn4+P//////7+/v/WZmZv9RUVH/ZWVllwAAAA////8AAAAAAv///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAAv///wAAAAASKCsvlNbW1/3//////Pv5/v////78/P3+hYWG/xgYGJ////8A////AKqqqgP///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AgICABP///wD///8AsaeitMDd9/+HtuD97vP4+f////+Pj47vAAAANP///wAgICAI////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AAD//wH///8AJNv/B1LJ60tMgZ7PI4rD/wCNyP9ips35kai8/1hYWusAAAAz////AAAAAAb///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAP//Af///wD///8ANKbWiiKn3f8modT/KaHX/gCMz/wAa6v/ADNR/xoAAGL///8AAAAABP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AQIC/BP///wByeI0xRo2+9B6a3v8xlNP9WJK//oSWp/4tMDT/AAAAjP///wD///8AAAAAAf///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wCqqqoD////AKylmkexr678eJKp/2+Elf3DwLz/xsPA/Dk3NP8AAACX////AP///wAAAAAC////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AKqqqgP///8AZ2tvRYyLiv1oYVr/Qzsy/Hx8fPp1dnf9AAAA/wQAAHb///8AAAAAA////wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8AAAAAA////wAAAAAZAAAA5C8yNP8wMjT8AAAA/QAAAP8VFRXdJiYmIv///wAzMzMF////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wA1NTVqNTU1/A8PD/8XFxb/ISEh3SEhIUb///8AAAAAAv///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wAAAAAB////AP///wAHBwdHHR0dhBgYGHYcHBwk////AP///wAAAAAB////AP///wD///8A////AP///wD///8A////AP///wD///8A////AP///wD///8A/////////////g///wIf//+DP///8z////M////wH///4B///+AP///AD///wAf//4AH//8AB///AAf//wAH//+AB///gAf//8AH///AB///wA///8AP///gH///8D////A////gP///8B////Af///wP///8D////h////9//8=";

var NOTIFICATION_ICON = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAALDUlEQVR42tVbB1hVRxb+L1VUFFDWHhdbsMSuQWU1BCuWDajoWrCxKmI3Yu9lLVFsyJq4doyi4q4iGpVl44eKvcQEa4yVKAhYAKkv5zzATx733nfv8z2ee77vfANzzsy7578zZ86cmSugBKhlN7+yVHgQuxE3IXYhrkJcpkAljTiB+D7xNeI44piLR3e8MfWzCSY02pqKvxIPI/YktlXZRSZxNPFW4v8QGNn/FwCQ4WyoP/F04upG6vYx8TLizQRE5kcLABnPbzqUuK4x+32P7hAHEAjRHxUABcN9JfF4Y/UpQxridcRTjTEtPvhhyXhHKg4Ru5vYcF2KJe5FIKSYDQAy3pmKE8j37OYgXjE6EQiJJQ4AGW9HRQzx52YyvpDOEXsQCBklDcAuKgaa2fhCCiMABpUYAGR8Pyr2SMmtrazg1qIRmjdyRY2qlVC2jB0y3mbi8k+3cPjEKaS+Eo9vbKyt4PVlO7i3bqpt8yYtA4+ePsPlGzcRd+kGsnNy5B6rP4Gw1+QAFDi928QVxeTeXb/AyEHeqOjkINr+9Zt0rAzdiaMxZ4rUN21YD/Mm/x3Vq/xJtN2LlJfYtCsCB4/+T+rRkojrqXWKhgDAAck03XoLQcDcyf7o7ql/MdBoNAiLOIb1W/Yij/4e1LsbAof2hZWlpd62UdGnsSD4O+TlacTEywmA6SYDgIwvT8UjYntd2ZghfTCsX09VYMbf/Q15uXlo+GktVe22hUciZNs+MdFr4hoEwktTARBIxQbd+lo1q2H3hkWwVPAGjUG5ubkYOHYu7j14LCYeSwCEmAqA01S01a2fN8kfPTr9pUSML6TIk7FYsPo7MdEZAqCd0QEg49np/U5c5DVbWlrgxPchsC9bukQBeJOWjo79x2pHgw5xRWUCIcnYAHhTEaFb38i1NraunluixhfSiCmLcD3+rpjIhwA4aGwAFlExW7f+q64dMGv8cLMAsHT9VqllcTEBMMfYALDb7aNbH+DXG8P795Jtm5ySiivXf0GNalVQr46LrO7tu/fx6EkCmjVuACdHB1ndrXsPY+P2/WKi/QRAX2MDwGmqYnF/0Bg/9O3hKdnuzPnLCJg8G+np+aH6iMG+mD4pQFR3WXAo/rUzXPt36dJ2CF29GG1bN5fse/+RaCwP2SEmOkcAuBkbAE5G1NGtnzFuKHy6eUi269p7CO7df1ik7kj4lmIjgd98d9+iU6m2yyc4dmC7ZN88/HkaiNBdAkBRUkYNABwAFUtxTQ0YDN+eHSXbNXTrjKysonmLfwYvgWeHoqtp9I9nMHrSrCJ1NjbW+DnuuGTf+yKjsWKj6Ah4TADUMBoAZHwpKji6stGVjSYfMELGB4ycOBMxp86++5+Hdszh3cXmN/sJj54D3k0VJo/2bfDtmqWSfW/ZcwihOw6IibKIyxMIb40FQGcqfhCT9enuiWmBfpJtk5JTMHPhSpyOu6h1gnOnTZCc1+wvFi5fq3WC7dxaYuncqbSpcpTsm98+jwIJ6kIASA8flQBw+BsoJnNr8RnWL/paSTfF6O25WG1Z6nPDsmnj53yDs5d+khKHEABjPxgAMp51OOiuKiZ3ruCIqJ1rVD149p14vA7bgqxLcdr/bVq4wX7gcFjXra+qn+5+E/E8SXL3+5S4OoGgketDCQDsrU7L6fywex2cHMrL9pOXkYHMuFPIOBmFrOuXRXVsGjeHXUcv2Lq1h4WdnWx/yamv0GXAOH2P344AOCOnoASA5VQESclrVquM3RsXw8baWraf3BeJSPTvB+ToyWRbWcN5815YVnCWVcvKzsaAwDl48DhBTm0FATBNTkEJADep+FRKvjgoAF2+UBRz4PWuzUjbu11Wp0y/IbAf5K+ov+M/xmHW8lA5lVsEgKvBAJDxbPhNKbmTQzkc2bEGVlbK8gCanBykzJsiOwUcF6yCYGWlqL+cnFytH+DpIEOuBMItQwGYScUSKbmPlwdmjB2q6GHfgfA2A6kU4maePVWk3rZNezhQyCyUslPV37IN23Eg6r9yKrMIAMlgQhIAMp5fA+81a0r++MxAeLq3VvXAWhA0GryNjUFaeH4UV8bXD6XcPSAI6pPU0bEXMH3pBjmVB8R1CATRlLIcABzdyE7YyB3BqFTRSfVDG5OeJyXTNJikT20IASAaM4sCUBD68tyXfPulbG1wKuJbg96aMYlHU4feo7TnDjLEo8BVLDSWAoB3JYvlenSpURXhm/5hVuMLyXf0DNx/+FSf2mwCoJg/KwYAGV+PiivEskk+TmVvC55nbtu1NGzyQty4eU+fWjpxMwLhtiQAZDyvZ+ye2+rrrWXj+ghdpuoMwmQUMGMZLl6LV6LKUWF7AuFdJlUXAH6l85X01KRBXWz+ZrYSVZOT/9dLcO2X20rV5xMAC4oBQMa3R/6lJEVRSL1anyBswyJz266lgWPn4PavD5Wq83LoSSCcegdAwUUHnvfVlPbi7OSAqF1rzW27lrwGT0TiC1Vnok+Q7w8SCwHgHPpXan+Yl0G7UmpvvxmX3mZmor33SGjUN/03AeAtkPG+9I/qc3Wm7Wvno0FdF0OaGo3i79yH34T5hjbvxwDw0G9qSOsJ/v0xyKebWQEIiziKNZv3GNr8KgNwnv5oJSLkpSIS+RGh6J66dbOGCFkSBHNS4KwVOH/lZykx5zJ4O9wDOmeaBXRBKEh5dSL+krgScSrxdeJjNEcSSM6OkVPixYImvhRxaNsqVHKuYBbjnyUmo9fQydpLFiLElXxX4AnZwPeSuxI3JuZ09DNi3kKeUJoUvcCFmMyvjxfGDe9nFgDWbwnHjv1HpMQXyfhW+vpQCoBkXoA3RRGbV2iToyVJvOz5+AfRKpAlpSKbB1ALAJ+y8FV20dRPu1ZNEDx/UontDHkHOGl+ME5fuCalwv7LhQB4ZBQACkDguwHeUnJD7ggZSjKnwoV0kIz3UdKXGgD4ZPisXJugMYPRt0dHpV0aRDLngYXEzq8NAXDOqAAUgKA3Yhzg3UV75U1fmlwtZWXnYOO2fQg7eEyfqjbCU9qvWgD4PhufRcnmCuq41MCUUQO1W2Zj0MXr8Vi1KQx37+ud0rzn/4wA+NUkABSAwN8EKNoFtWjsqp0S7uQkbW1tlDR5R5nk3WPJye2LPIlL128qbTaBjF+n5ncMvSvMHqi3Un3eMLWg0cA5hNp/ro5qlZzh6FBOu4Qy8VKWkvoKT54l4t5vj2lvf4eMjteX59OlA2R8HzUNPgQA/trrJPK/AvsYiE9ZOxIAaSUCANNar+4OhzQOUS9h2caclpdD7tluwkuvqVGRqYa0VwVAQg93PiLneIBDTItMjYA1GmfEasqaxXh34Q0mComwFbR7gTxiDtl9qkTG6k0RGwoAO7/x79fxTx/Ps8cWTQVywRYlYnhpsnW48AKdLV6LGbCOAJhQYgAUUrLGErs1jojW2CPHRB+OWRHcnsJrDBBS4CTkSqmZFIAiU0BMJ5GAOKYphxiaFokwTjDkjGx40HDvKryCs7Thpp8COmDw1bBNxJXF5LxFvwsbXNGURrzGlnZStnih3Uvp+0kNKtBexgWZqC9kopmQjjrIgsw+iy9wjyKjDxlix4cAkCBlvBRlkNNMooFMK4fWX7zUWFzl+vJCXlOe1+XJ8Io0gewE1SnO3wmAKh89AAXEF65O0Aj5PhMCxxI0NjQd6Q3/DfmZKUO+NzYLAIVTgM8Ust5jDt/405Vk4ufIv63FuQQ+urlKD/pIT7+ce+AkLZ9RcsqZ/Q5/ScXn8PbI/wrd5j3mjyYNngJ/ADIW28orde2WAAAAAElFTkSuQmCC";

var autorefreshTickIntervalMillisecs = 1000;

var autorefreshEnabled = true;
var autorefreshIntervalSecs = 10;
var autorefreshLeftSecs = autorefreshIntervalSecs;
var autorefreshLabel = null;
var newCommentsCount = 0;
var originalDocumentTitle = '';
var delayBeforePreviewMillisecs = 0;
var delayAfterPreviewMillisecs = 800;
var responsesAddingInProcess = false;
var commentsInfo = [];
var currentPreview = null;
var pagesLimitForResponses = 20;
var showResponses = true;
var desktopNotificationsEnabled = false;

function Comment(element, id, link, referencedCommentID, author) {
  this.element = element;
  this.id = id;
  this.link = link;
  this.referencedCommentID = referencedCommentID;
  this.author = author;
}

function getCommentInfo(comment) {
  var commentLinkElement = $(
    comment.find(
      'div.msg-container > div.msg_body > div.reply'
    ).find('a:contains(Ссылка)').first()
  );
  var commentLink = commentLinkElement.attr('href');
  if (typeof commentLink === 'undefined') {
    return null;
  }

  var matches = commentLink.match(/.*cid=(\d+).*/);
  if (matches === null) {
    return null;
  }
  var commentID = matches[1];

  var referencedCommentID = null;
  var a = comment.find('div.title > a').first();
  if (a.length > 0) {
    var referencedCommentLink = a.attr('href');
    if (typeof referencedCommentLink !== 'undefined') {
      var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
      if (matches !== null) {
        referencedCommentID = matches[1];
      }
    }
  }

  var author = null;
  var creatorTag = comment.find('a[itemprop=creator]').first();
  if (creatorTag.length > 0) {
    author = creatorTag.text();
  } else {
    // anonymous
  }

  return new Comment(comment, commentID, commentLink, referencedCommentID, author);
}

function addCommentToCache(commentInfo) {
  var isNewComment = true;
  for (var i = 0; i < commentsInfo.length; ++i) {
    var cachedCommentInfo = commentsInfo[i];
    if (cachedCommentInfo.id === commentInfo.id) {
      commentsInfo[i] = cachedCommentInfo;
      isNewComment = false;
      break;
    }
  }
  if (isNewComment) {
    commentsInfo.push(commentInfo);
  }
}

function addResponsesLinksInternal(possibleAnswers) {
  for (var i = 0; i < possibleAnswers.length; ++i) {
    var comment = $(possibleAnswers[i]);
    var commentInfo = getCommentInfo(comment);
    if (commentInfo === null) {
      continue;
    }

    if (commentInfo.referencedCommentID !== null) {
      for (var j = 0; j < commentsInfo.length; ++j) {
        var cachedCommentInfo = commentsInfo[j];
        if (cachedCommentInfo.id === commentInfo.referencedCommentID) {
          if (cachedCommentInfo.element.find(
            'a.response[href="' + commentInfo.link + '"]'
          ).length > 0) {
            continue;
          }

          var responseTag = $('<a/>', {
            href: commentInfo.link,
            html: commentInfo.author ? commentInfo.author : '>>' + commentInfo.id,
            class: 'response',
            style: 'padding-left: 5px'
          });
          responseTag.click(function(e) {
            var comment = $('#comment-' + commentInfo.id).get(0);
            if (comment) {
              e.preventDefault();
              comment.scrollIntoView();
            }
          });

          var replyPanel = cachedCommentInfo.element.find('div.reply > ul').first();
          if (replyPanel.length === 0) {
            continue;
          }
          var responseBlock = replyPanel.find('.response-block').first();
          if (responseBlock.length === 0) {
            responseBlock = $('<li/>', {
              html: 'Ответы:',
              class: 'response-block'
            });
            replyPanel.append(' ').append(responseBlock);
          }

          setShowPreviewCallback(responseTag);
          responseBlock.append(responseTag);
        }
      }
    }

    addCommentToCache(commentInfo);
  }
}

function addResponsesLinks() {
  if (responsesAddingInProcess || !showResponses) {
    return;
  }
  responsesAddingInProcess = true;

  // Process current page
  var curPageComments = $('#comments').find('article.msg');
  addResponsesLinksInternal(curPageComments);

  // Process other pages
  var otherPagesRequests = [];
  do {
    var pagesNavBar = $('#comments').find('.nav').first();
    if (pagesNavBar.length === 0) {
      break;
    }

    var otherPagesLinksElements = pagesNavBar.find('a.page-number');
    for (var i = 0; i < otherPagesLinksElements.length; ++i) {
      var otherPageLinkElement = $(otherPagesLinksElements[i]);

      var otherPageLink = otherPageLinkElement.attr('href');
      if (typeof otherPageLink === 'undefined') {
        continue;
      }

      otherPagesRequests.push($.get(otherPageLink));
    }
  } while (false);

  if (otherPagesRequests.length === 0) {
    responsesAddingInProcess = false;
  } else {
    var defer = $.when.apply($, otherPagesRequests);
    defer.done(function() {
      $.each(arguments, function(index, responseData) {
        var possibleAnswers = $(responseData[0]).find('#comments').find('article.msg');
        addResponsesLinksInternal(possibleAnswers);
      });
    }).always(function() {
      responsesAddingInProcess = false;
    });
  }
}

function getOffset(element, offsetType) {
  var offset = 0;
  while (element) {
    offset += element[offsetType];
    element = element.offsetParent;
  }
  return offset;
}

function getScreenWidth() {
  return document.body.clientWidth || document.documentElement.clientWidth;
}

function getScreenHeight() {
  return  window.innerHeight || document.documentElement.clientHeight;
}

function removeElement(element) {
  if (!element) {
    return;
  }

  if (element.parentNode) {
    element.parentNode.removeChild(element);
  }
}

function removePreviews(e) {
  currentPreview = e.relatedTarget;
  if (!currentPreview) {
    return;
  }

  while (true) {
    if (/^preview/.test(currentPreview.id)) {
      break;
    } else {
      currentPreview = currentPreview.parentNode;
      if (!currentPreview) {
        break;
      }
    }
  }

  setTimeout(function() {
    if (currentPreview === null) {
      $('article.msg[id*="preview-"]').remove();
    } else {
      while (currentPreview.nextSibling) {
        if (!/^preview/.test(currentPreview.nextSibling.id)) {
          break;
        }
        removeElement(currentPreview.nextSibling);
      }
    }
  }, delayAfterPreviewMillisecs);
}

function showCommentInternal(commentElement, commentID, e) {
  currentPreview = commentElement.get(0);

  // Avoid duplicated IDs when the original comment was found on the same page
  commentElement.attr('id', 'preview-' + commentID);

  // From makaba
  var hoveredLink = e.target;
  var x = getOffset(hoveredLink, 'offsetLeft') + hoveredLink.offsetWidth / 2;
  var y = getOffset(hoveredLink, 'offsetTop');
  var screenWidth = getScreenWidth();
  var screenHeight = getScreenHeight();
  if (e.clientY < screenHeight * 0.75) {
    y += hoveredLink.offsetHeight;
  }
  commentElement.attr(
    'style',
      'position: absolute;' +
      // There are no limitations for the 'z-index' in the CSS standard,
      // so it depends on the browser. Let's just set it to 300
      'z-index: 300;' +
      'border: 1px solid grey;' +
      (
        x < screenWidth / 2
        ? 'left: ' + x
        : 'right: ' + parseInt(screenWidth - x + 2)
      ) + 'px;' +
      (
        e.clientY < screenHeight * 0.75
        ? 'top: ' + y
        : 'bottom: ' + parseInt(screenHeight - y - 4)
      ) + 'px;'
  );

  // If this comment contains link to another comment,
  // set the 'mouseover' hook to that 'a' tag
  var a = commentElement.find('div.title > a').first();
  if (a.length > 0) {
    setShowPreviewCallback(a);

    var referencedCommentID = -1;
    var referencedCommentLink = a.attr('href');
    if (typeof referencedCommentLink !== 'undefined') {
      var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
      if (matches !== null) {
        referencedCommentID = matches[1];
      }
    }
    if (referencedCommentID !== -1) {
      a.click(function(e) {
        var comment = $('#comment-' + referencedCommentID).get(0);
        if (comment) {
          e.preventDefault();
          comment.scrollIntoView();
        }
      });
    }
  }

  commentElement.mouseenter(function() {
    if (!currentPreview) {
      currentPreview = this;
    }
  });

  commentElement.mouseleave(function(e) {
    removePreviews(e);
  });

  // Note that we append the comment to the '#comments' tag,
  // not the document's body
  // This is because we want to save the background-color and other styles
  // which can be customized by userscripts and themes
  $('#comments').find('article.msg').last().after(commentElement);
}

function findCachedComment(id) {
  for (var i = 0; i < commentsInfo.length; ++i) {
    var commentInfo = commentsInfo[i];
    if (commentInfo.id === id) {
      return commentInfo.element;
    }
  }
  return null;
}

function showPreview(e) {
  // Extract link to the comment
  var href = $(e.target).attr('href');
  if (typeof href === 'undefined') {
    return;
  }

  // Extract comment's ID from the 'href' attribute
  var matches = href.match(/.*cid=(\d+).*/);
  if (matches === null) {
    return;
  }
  var commentID = matches[1];

  // Let's reduce an amount of GET requests
  // by searching a cache of comments first
  var commentElement = findCachedComment(commentID);
  if (commentElement !== null) {
    showCommentInternal(
      // Without the 'clone' call we'll just move the original comment
      commentElement.clone(true),
      commentID,
      e
    );
    return;
  }

  // Get an HTML containing the comment
  $.get(href, function(data) {
    // Search for the comment on the requested page
    var commentElementSelector = 'article.msg[id=comment-' + commentID + ']';
    var commentElement = $(data).find(
      commentElementSelector
    ).first();
    if (commentElement.length === 0) {
      return;
    }

    showCommentInternal(
      commentElement,
      commentID,
      e
    );
  });
}

function setShowPreviewCallback(elements) {
  elements.hover(function(e) {
    $(this).data('timeout',
      window.setTimeout(
        function() {
          showPreview(e);
        }, delayBeforePreviewMillisecs
      )
    );
  }, function(e) {
    clearTimeout($(this).data('timeout'));
    removePreviews(e);
  });
}

function autorefreshTick() {
  if (!autorefreshEnabled) {
    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
    return;
  }

  autorefreshLeftSecs -= 1;
  if (autorefreshLeftSecs !== 0) {
    autorefreshLabel.find('span').text(
      'Автообновление через ' + autorefreshLeftSecs + ' с...'
    );
    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
    return;
  }

  autorefreshLabel.find('span').text(
    'Обновление...'
  );

  var comments = $('#comments');

  $.get(document.location.href, function(data) {
    var prevNewCommentsCount = newCommentsCount;

    var messages = $(data).find('article.msg');
    for (var i = 0; i < messages.length; ++i) {
      var message = $(messages[i]);
      var messageID = message.attr('id');
      var isNewMessage = $('#' + messageID).length === 0;
      if (!isNewMessage) {
        continue;
      }

      ++newCommentsCount;

      var a = message.find('div.title > a').first();
      if (a.length > 0) {
        setShowPreviewCallback(a);
        var referencedCommentID = -1;
        var referencedCommentLink = a.attr('href');
        if (typeof referencedCommentLink !== 'undefined') {
          var matches = referencedCommentLink.match(/.*cid=(\d+).*/);
          if (matches !== null) {
            referencedCommentID = matches[1];
          }
        }
        if (referencedCommentID !== -1) {
          a.click(function(e) {
            var comment = $('#comment-' + referencedCommentID).get(0);
            if (comment) {
              e.preventDefault();
              comment.scrollIntoView();
            }
          });
        }
      }

      comments.append(message);
    }

    // Remove all nav. bars related to the pagination
    // (there are also nav. bars related to threads, but let's just skip them)
    var pagesBefore = 0;
    var currentNavBars = $(document.body).find('.nav');
    for (var i = 0; i < currentNavBars.length; ++i) {
      var currentNavBar = $(currentNavBars[i]);
      var pagesElementsCount = currentNavBar.find('.page-number').length;
      var isPagesNavBar = pagesElementsCount !== 0;
      if (isPagesNavBar) {
        pagesBefore = pagesElementsCount;
        currentNavBar.remove();
      }
    }

    var pagesAfter = 0;
    var newPagesNavBar = $(data).find('#comments').find('.nav').has('.page-number').first();
    if (newPagesNavBar.length === 0) {
      comments.append(autorefreshLabel);
    } else {
      pagesAfter = newPagesNavBar.find('.page-number').length;
      comments.find('.msg').first().before(newPagesNavBar.clone());
      comments.append(autorefreshLabel);
      comments.append(newPagesNavBar.clone());
    }

    if (pagesAfter > pagesBefore || newCommentsCount > prevNewCommentsCount) {
      faviconNotify(true);
      if (desktopNotificationsEnabled) {
        showNotification(
          'Новые ответы в теме "' + originalDocumentTitle + '"'
        );
      }
    }

    if (pagesAfter > pagesBefore) {
      document.title = '(!) ' + originalDocumentTitle;
    } else if (newCommentsCount !== 0) {
      document.title = '(' + newCommentsCount + ') ' + originalDocumentTitle;
    }

    disableResponsesForThreadsExceedingPagesLimit();
    addResponsesLinks();
  }).always(function() {
    autorefreshLeftSecs = autorefreshIntervalSecs;
    autorefreshLabel.find('span').text(
      'Автообновление через ' + autorefreshLeftSecs + ' с...'
    );

    setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
  });
}

function onAutorefreshCheckboxChange() {
  autorefreshEnabled = $(this).is(':checked');
  if (!autorefreshEnabled) {
    autorefreshLabel.find('span').text(
      'Автообновление'
    );
  } else {
    autorefreshLeftSecs = autorefreshIntervalSecs;
    autorefreshLabel.find('span').text(
      'Автообновление через ' + autorefreshLeftSecs + ' с...'
    );
  }
}

function setAutorefresher() {
  originalDocumentTitle = document.title;

  autorefreshLabel = $('<label/>', {
    // Without the inner 'span' tag
    // the next call to the 'text' function will remove the 'checkbox' control
    html: autorefreshEnabled
      ? '<span>Автообновление через '
        + autorefreshLeftSecs
        + ' с...</span>'
      : '<span>Автообновление</span>'
  });
  var autorefreshCheckbox = $('<input/>', {
    type: 'checkbox',
    checked: autorefreshEnabled
  });
  autorefreshCheckbox.change(onAutorefreshCheckboxChange);
  autorefreshLabel.prepend(autorefreshCheckbox);
  $('#comments').append(autorefreshLabel);

  $(window).scroll(function() {
    if (newCommentsCount !== 0) {
      faviconNotify(false);
      document.title = originalDocumentTitle;
      newCommentsCount = 0;
    }
  });

  // setInterval calls may overlap so let's call setTimeot each time manually
  setTimeout(autorefreshTick, autorefreshTickIntervalMillisecs);
}

function removePreviewsOnClick() {
  $(window).click(function() {
    var previewSelector = 'article.msg[id*="preview-"]';
    if (currentPreview === null) {
      $(previewSelector).remove();
    }
  });
}

var archivedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема перемещена в архив.';
var deletedTopicString = 'Вы не можете добавлять комментарии в эту тему. Тема удалена.';

function disableAutorefreshForClosedTopics() {
  var infoblock = $('.infoblock').text();

  if (infoblock.indexOf(archivedTopicString) !== -1 ||
      infoblock.indexOf(deletedTopicString) !== -1) {
    autorefreshEnabled = false;
  }
}

var favicon = null;

function getFavicon() {
  if (favicon === null) {
    favicon = $('head > link[rel~="icon"]');
  }
  return favicon;
}

function faviconNotify(flag) {
  getFavicon().attr('href', flag ? FAVICON_NOTIFY : FAVICON_ORIGINAL);
}

function isNumeric(num){
  return !isNaN(num);
}

function disableResponsesForThreadsExceedingPagesLimit() {
  var pagesNavBar = $('#comments').find('.nav').has('.page-number').first();
  var pagesCount = 0;
  if (pagesNavBar.length !== 0) {
    var pagesElements = pagesNavBar.find('.page-number');
    for (var i = 0; i < pagesElements.length; ++i) {
      var pageElement = $(pagesElements[i]);
      var pageLabel = pageElement.text();
      if (isNumeric(pageLabel)) {
        ++pagesCount;
      }
    }
  }
  showResponses = pagesCount <= pagesLimitForResponses;
}

function showNotification(text) {
  if (!Notification) {
    return;
  }

  if (Notification.permission !== 'granted') {
    Notification.requestPermission();
  } else {
    var notification = new Notification('lorify', {
      icon: NOTIFICATION_ICON,
      body: text,
    });
  }
}

function grantNotificationsPermission() {
  if (!Notification) {
    return;
  }

  if (Notification.permission !== 'granted') {
    Notification.requestPermission();
  }
}

function onDOMReady() {
  if (desktopNotificationsEnabled) {
    grantNotificationsPermission();
  }

  disableResponsesForThreadsExceedingPagesLimit();
  removePreviewsOnClick();
  setShowPreviewCallback($('div.title > a'));
  disableAutorefreshForClosedTopics();
  setAutorefresher();
  addResponsesLinks();
}

$(function() {
  onDOMReady();
});
